<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>American Express Vacations</title>
	<link href="css/styles.min.css" rel="stylesheet">
</head>
<body>
	<!--
		Start of DoubleClick Floodlight Tag: Please do not remove
		Activity name of this tag: Retargeting
		URL of the webpage where the tag is expected to be placed: http://www.seetorontonow.com
		This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
		Creation Date: 07/19/2016
	-->

	<script type="text/javascript">
		var axel = Math.random() + "";
		var a = axel * 10000000000000;
		document.write('<iframe src="https://5880041.fls.doubleclick.net/activityi;src=5880041;type=invmedia;cat=mszzawpi;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
	</script>

	<noscript>
		<iframe src="https://5880041.fls.doubleclick.net/activityi;src=5880041;type=invmedia;cat=mszzawpi;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
	</noscript>

	<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

	<nav class="navbar navbar-default mobile-nav visible-xs">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="/" class="navbar-brand">
					<img src="img/logo.png" alt="logo" class="img-responsive">
				</a>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="./">Home</a></li>
				<li><a href="/experiencias">Experiencias</a></li>
				<li><a href="/reserva">Cotizar</a></li>
			</ul>
		</div>
	</nav>
	<header>
		<nav class="hidden-xs">
			<div class="container">
				<div class="row main-menu">
					<div class="col-sm-3">
						<a href="/"><img src="img/logo-white.png" alt="logo" class="img-responsive"></a>
					</div>
					<div class="col-sm-9">
						<ul class="list-inline text-right">
							<li><a href="./">HOME</a></li>
							<li><a href="/experiencias">EXPERIENCIAS</a></li>
							<li><a href="/reserva">COTIZAR</a></li>
						</ul>	
					</div>
				</div>
			</div>
		</nav>
	</header>