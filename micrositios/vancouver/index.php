<?php require_once('header.php'); ?>

	<div class="container experiencia-home">
		<div class="row">
			<article class="col-sm-6">
				<a href="opt1.php">
					<div class="thumbnail">
						<img src="img/van1.jpg" alt="">
					</div>
					<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
					<div class="description">
						<h1 class="title">PAQUETE PRE/POST CRUCERO EN VANCOUVER</h1>
					</div>
				</a>
			</article>
			<article class="col-sm-6">
				<a href="opt2.php">
					<div class="thumbnail">
						<img src="img/van2.jpg" alt="">
					</div>
					<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
					<div class="description">
						<h1 class="title">VANCOUVER Y VICTORIA</h1>
						<h3 class="quote">Disfruta de la hotelería Fairmont</h3>
					</div>
				</a>
			</article>
			<!-- <div class="clearfix"></div> -->
			<article class="col-sm-6">
				<a href="opt3.php">
					<div class="thumbnail">
						<img src="img/van3.jpg" alt="">
					</div>
					<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
					<div class="description">
						<h1 class="title">VANCOUVER - VICTORIA ROCOSAS - CALGARY</h1>
						<h3 class="quote">Lo mejor de las Montañas Rocosas</h3>
					</div>
				</a>
			</article>
			<article class="col-sm-6">
				<a href="opt4.php">
					<div class="thumbnail">
						<img src="img/van4.jpg" alt="">
					</div>
					<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
					<div class="description">
						<h1 class="title">VANCOUVER - JASPER - LAKE LOUISE - ROCKY MOUNTAINEER</h1>
						<h3 class="quote">Considerado uno de los mejores trenes escénicos</h3>
					</div>
				</a>
			</article>
			<article class="col-sm-6">
				<a href="opt5.php">
					<div class="thumbnail">
						<img src="img/van5.jpg" alt="">
					</div>
					<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
					<div class="description">
						<h1 class="title">EXTENSIÓN A SONORA RESORT DESDE VANCOUVER</h1>
						<h3 class="quote">Osos, águilas, pesca, vida marina y más</h3>
					</div>
				</a>
			</article>
		</div>
	</div>

<?php require_once('footer.php'); ?>