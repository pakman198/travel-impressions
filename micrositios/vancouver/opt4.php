<?php require_once('header.php'); ?>

	<main class="container experiencia-detail">
	    <section class="header">
	        <div class="col-sm-8">
	            <div class="heading">
	            	<div class="title">
	            		VANCOUVER - JASPER - LAKE LOUISE - ROCKY MOUNTAINEER
	            		<div class="subtitle">Considerado uno de los mejores trenes escénicos</div>
	            	</div>
	                <div class="pic-container">
	                	<img src="img/van4.jpg" alt="" class="img-responsive">
	                	<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-4">
	            <div class="excerpt">
	                <p class="strong">8 NOCHES</p>
	                <p>
	                	Salidas: lunes <br>
	                	Septiembre: 5, 12, 19, 27
	                </p>
	                <p class="strong">Código: YVR-37T</p>
	            </div>
	            <div class="promo">
	            	<p>paga a 6 meses sin intereses</p>
	            	<img src="img/tarjetas.jpg" alt="" class="img-responsive">
	            </div>
	        </div>
	    </section>
	    <section class="description">
	        <div class="col-sm-12">
	            <h3>Costo total por persona / Temporada baja (Junio 12 y 19)</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$5,279.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Triple</td>
	            			<td>$4,892.00</td>
	            		</tr>
	            		<tr>
	            			<td>Cuádruple</td>
	            			<td>$4,712.00</td>
	            		</tr>
	            		<tr>
	            			<td>Menor (Hasta 12)</td>
	            			<td>$3,760.00</td>
	            		</tr>
	            	</table>
	            </div>

	            <h3>Costo total por persona / Temporada alta</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$5,849.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Triple</td>
	            			<td>$5,001.00</td>
	            		</tr>
	            		<tr>
	            			<td>Cuádruple</td>
	            			<td>$4,792.00</td>
	            		</tr>
	            		<tr>
	            			<td>Menor (Hasta 12)</td>
	            			<td>$3,760.00</td>
	            		</tr>
	            	</table>
	            </div>

	            <h3>*Suplemento GOLD LEAF SERVICE</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$889.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Triple</td>
	            			<td>$889.00</td>
	            		</tr>
	            		<tr>
	            			<td>Cuádruple</td>
	            			<td>$889.00</td>
	            		</tr>
	            		<tr>
	            			<td>Menor (Hasta 12)</td>
	            			<td>$889.00</td>
	            		</tr>
	            	</table>
	            </div>

	            <h3>Hoteles programados o similares</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>3</td>
	            			<td>VANCOUVER</td>
	            			<td>Fairmont Hotel Vancouver (Superior-Lujo)</td>
	            		</tr>
	            		<tr>
	            			<td>1</td>
	            			<td>KAMLOOPS</td>
	            			<td>South Thompson Inn (Rancho)</td>
	            		</tr>
	            		<tr>
	            			<td>2</td>
	            			<td>JASPER</td>
	            			<td>Fairmont Jasper Park Lodge (1) (Lodge)</td>
	            		</tr>
	            		<tr>
	            			<td>1</td>
	            			<td>LAKE LOUISE</td>
	            			<td>Fairmont Chateau Lake Louise (2) (Lujo)</td>
	            		</tr>
	            		<tr>
	            			<td>1</td>
	            			<td>CALGARY</td>
	            			<td>International Hotel (Primera)</td>
	            		</tr>
	            	</table>
	            </div>
	            
				<h3>Incluye</h3>

				<ul>
					<li>
						Traslados Aeropuerto - Hotel - Aeropuerto
					</li>
					<li>
						8 Desayunos americanos
					</li>
					<li>
						1 Cena en el Rancho South Thompson Inn
					</li>
					<li>
						1 Almuerzo en el Chalet de Maligne Lake
					</li>
					<li>
						Transporte en grupos reducidos con chofer/guía en español
					</li>
					<li>
						Visitas incluidas: Tour de la Ciudad de Vancouver, Teleférico Hells Gate, Ice Explorer, TREN ROCKY MOUNTAINEER (cotizado en Silver Leaf)
					</li>
					<li>
						Maleteros (1 pieza de equipaje por persona)
					</li>
					<li>
						Bolsa porta documentos por persona
					</li>
				</ul>

				<h5>CATEGORIA SILVER LEAF</h5>
				<p>
					Vagón de un piso con techo semi-panorámico. Manejo de equipaje hasta su hotel. Alimentos ligeros servidos en su asiento.
				</p>

				<h5>CATEGORIA GOLD LEAF</h5>
				<p>
					Vagón de dos pisos con techo panorámico y restaurante en la parte inferior del vagón. Manejo de equipaje hasta su hotel. Selección de desayuno y almuerzo gourmet. Bebidas alcohólicas y snacks incluidos durante todo el recorrido. Servicio personalizado.
				</p>

				<p class="legales">
					Legales <br>
					Hoteleria en hoteles mencionados o similares. Tipo de habitación: estándar. En caso de preferir habitaciones superiores, favor de consultar suplemento. Fairmont Chateau Lake Louise la habitación es estándar y no tiene vista al lago. Precio de niño aplica cuando comparte habitación con 2 adultos. *Fechas coincidentes con la Estampida de Calgary (hay suplemento). 1) El Fairmont Jasper Park Lodge se reemplazará por el Tekarra Lodge en habitación Cabin (consulte salida). (2) El Fairmont Chateau Lake Louise en Julio 24 y Agosto 21, se reemplazará por el Post Hotel & Spa (Relais & Chateaux Property).

				</p>
				
				<h4>CONSULTE ITINERARIO DETALLADO</h4>
				
				<div class="clearfix">
					<img src="img/bc.png" alt="" class="img-responsive pull-right">
				</div>

				<p class="tel">
					Llámanos: (55) 5901 2000 | 01 800 502 1500
				</p>
				<p class="cotizar">
					<a href="/reserva">Cotizar</a>
				</p>
	        </div>
	    </section>
	</main>

<?php require_once('footer.php'); ?>