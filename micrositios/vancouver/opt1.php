<?php require_once('header.php'); ?>

	<main class="container experiencia-detail">
	    <section class="header">
	        <div class="col-sm-8">
	            <div class="heading">
	            	<div class="title">PAQUETE PRE/POST CRUCERO EN VANCOUVER</div>
	                <div class="pic-container">
	                	<img src="img/van1.jpg" alt="" class="img-responsive">
	                	<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-4">
	            <div class="excerpt">
	                <p class="strong">3 NOCHES</p>
	                <p>Salidas: hasta el 30 de septiembre 2016</p>
	                <p class="strong">Código: YVR-10A</p>
	            </div>
	            <div class="promo">
	            	<p>paga a 6 meses sin intereses</p>
	            	<img src="img/tarjetas.jpg" alt="" class="img-responsive">
	            </div>
	        </div>
	    </section>
	    <section class="description">
	        <div class="col-sm-12">
	            <h3>Costo total por persona</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$1,054.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Triple</td>
	            			<td>$866.00</td>
	            		</tr>
	            		<tr>
	            			<td>Cuádruple</td>
	            			<td>$771.00</td>
	            		</tr>
	            		<tr>
	            			<td>Menor (Hasta 12)</td>
	            			<td>$276.00</td>
	            		</tr>
	            	</table>
	            </div>
	            
				<h3>Incluye</h3>

				<ul>
					<li>
						3 noches de hospedaje en el Shangri-La Vancouver Hotel en ocupación doble superior
					</li>
					<li>
						 Traslados Aeropuerto - Hotel
					</li>
					<li>
						Tour por la ciudad de Vancouver terminando en el muelle (para abordar su crucero)
					</li>
					<li>
						Traslado Muelle - Hotel
					</li>
					<li>
						Traslado Hotel - Aeropuerto
					</li>
					<li>
						Asistencia en español
					</li>
					<li>
						Bolsa porta documentos por persona
					</li>
				</ul>

				<h4>contamos con gran variedad de hoteles</h4>

				<p class="legales">
					Legales <br>
					En caso de preferir otro tipo de habitaciones, favor de consultar suplemento. Precio del niño aplica cuando comparte habitación con 2 adultos. 
				</p>

				<div class="clearfix">
					<img src="img/bc.png" alt="" class="img-responsive pull-right">
				</div>

				<p class="tel">
					Llámanos: (55) 5901 2000 | 01 800 502 1500
				</p>
				<p class="cotizar">
					<a href="/reserva">Cotizar</a>
				</p>
	        </div>
	    </section>
	</main>

<?php require_once('footer.php'); ?>