<?php require_once('header.php'); ?>

	<main class="container experiencia-detail">
	    <section class="header">
	        <div class="col-sm-8">
	            <div class="heading">
	            	<div class="title">
	            		EXTENSIÓN A SONORA RESORT DESDE VANCOUVER
	            		<div class="subtitle">Osos, águilas, pesca, vida marina y más</div>
	            	</div>
	                <div class="pic-container">
	                	<img src="img/van5.jpg" alt="" class="img-responsive">
	                	<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-4">
	            <div class="excerpt">
	                <p class="strong">3 NOCHES</p>
	                <p>Salidas: domingos, martes o jueves</p>
	                <p class="strong">Código: TIDOM</p>
	            </div>
	            <div class="promo">
	            	<p>paga a 6 meses sin intereses</p>
	            	<img src="img/tarjetas.jpg" alt="" class="img-responsive">
	            </div>
	        </div>
	    </section>
	    <section class="description">
	        <div class="col-sm-12">
	        	<div class="row">
	        		<div class="col-sm-8">
	        			<p class="italic">
	        				Imagine un lugar alejado del ruido y de las ciudades. Un sitio en armonía con el medio ambiente y con los lujos que solo la madre naturaleza nos puede ofrece. Con la mejor pesca de salmón, este elegante resort ofrece instalaciones que lo hace único.
	        			</p>		
	        		</div>
	        	</div>

	            <h3>Temporada alta / Julio 1 - Septiembre 30</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$4,024.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Niños (5-19)</td>
	            			<td>$3,101.00</td>
	            		</tr>
	            	</table>
	            </div>

				<h3>Temporada baja / Octubre 1 - 31</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$2,731.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Niños (5-19)</td>
	            			<td>$2,524.00</td>
	            		</tr>
	            	</table>
	            </div>
	            
				<h3>Incluye</h3>

				<ul>
					<li>
						Habitacioón Gold Deluxe Ocean View
					</li>
					<li>
						 Las habitaciones cuentan con chimenea, televisón satélite, albornoces y pantunfas
					</li>
					<li>
						Bebidas alcohólicas y no alcohólicas en el minibar incluidas
					</li>
					<li>
						Todas las comidas (desayuno, almuerzo, cena y entremeses) durante toda la estadía
					</li>
					<li>
						Todas las bebidas (incluyendo bebidas alcoholicas)
					</li>
					<li>
						Uso de todas las facilidades del resort, incluyendo piscinas, jacuzzis, sauna, canchas de tenis, billares y teatro
					</li>
					<li>
						Uso de bicicletas de montaña
					</li>
					<li>
						Acceso al área de gimnasio Island Currents Wellness Centre
					</li>
					<li>
						Uso del business centre
					</li>
					<li>
						Acces a internet en la propiedad del resort
					</li>
					<li>
						TRANSPORTE AL RESORT EN HELICÓPTERO DESDE VANCOUVER
					</li>
					<li>
						Incluye las propinas sección hotelería
					</li>
					<li>
						Bolsa porta documentos por persona
					</li>
				</ul>

				<p class="legales">
					Legales <br>
					Se recomienda la compra de seguro de viajero. Traer camara de fotos y/o filmadora y tripoide. Ropa abrigada, sombrero, guantes, bufanda y ropa interior térmica. No se puede fumar en las habitaciones y aplica una penalización de $500 CAD. Deposito del 35% (no reembolsable en caso de cancelación) requerido al momento de la confirmación de la reserva. Consulta políticas de cancelación. Puedes agregar actividades opcionales como: Pesca de salmón, Tour de glaciares en helicóptero, Kayak guiado, Tour de Osos Grizzly, Golf en Storey Creek.
				</p>

				<h4>COMPLEMENTA TU EXPERIENCIA HOSPEDÁNDOTE EN VANCOUVER</h4>

				<div class="clearfix">
					<img src="img/bc.png" alt="" class="img-responsive pull-right">
				</div>

				<p class="tel">
					Llámanos: (55) 5901 2000 | 01 800 502 1500
				</p>
				<p class="cotizar">
					<a href="/reserva">Cotizar</a>
				</p>
	        </div>
	    </section>
	</main>

<?php require_once('footer.php'); ?>