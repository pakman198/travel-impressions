<?php require_once('header.php'); ?>

	<main class="container experiencia-detail">
	    <section class="header">
	        <div class="col-sm-8">
	            <div class="heading">
	            	<div class="title">
	            		VANCOUVER Y VICTORIA
	            		<div class="subtitle">Disfruta de la hotelería Fairmont</div>
	            	</div>
	                <div class="pic-container">
	                	<img src="img/van2.jpg" alt="" class="img-responsive">
	                	<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-4">
	            <div class="excerpt">
	                <p class="strong">5 NOCHES</p>
	                <p>
	                	Salidas: viernes <br>
	                	Septiembre: 2, 9, 16, 23
	                </p>
	                <p class="strong">Código: YVR-10 PLUS</p>
	            </div>
	            <div class="promo">
	            	<p>paga a 6 meses sin intereses</p>
	            	<img src="img/tarjetas.jpg" alt="" class="img-responsive">
	            </div>
	        </div>
	    </section>
	    <section class="description">
	        <div class="col-sm-12">
	            <h3>5 días / Costo total por persona</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$1,735.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Triple</td>
	            			<td>$1,452.00</td>
	            		</tr>
	            		<tr>
	            			<td>Cuádruple</td>
	            			<td>$1,311.00</td>
	            		</tr>
	            		<tr>
	            			<td>Menor (Hasta 12)</td>
	            			<td>$888.00</td>
	            		</tr>
	            	</table>
	            </div>
	            
				<h3>Incluye</h3>

				<ul>
					<li>
						4 noches de hospedaje en el Fairmont Hotel en VANCOUVER en base ocupación doble estándar
					</li>
					<li>
						 1 noche de hospedaje en el Fairmont Empress en VICTORIA Hotel en base ocupación doble estándar
					</li>
					<li>
						5 Desayunos americanos
					</li>
					<li>
						Traslados Aeropuerto - Hotel - Aeropuerto
					</li>
					<li>
						Tour de ciudad en Vancouver
					</li>
					<li>
						Tour en Victoria (incluye ferry de ida y de regreso)
					</li>
					<li>
						Entrada a los Jardines Butchard
					</li>
					<li>
						Asistencia en español
					</li>
					<li>
						Bolsa porta documentos por persona
					</li>
				</ul>

				<h4>
					Complementa tu estancia con los siguientes opcionales: Visita del Norte de Vancouver, Vuelo panorámico en hidroavión, FlyOver Canadá.
				</h4>

				<p class="legales">
					Legales <br>
					En caso de preferir habitaciones superiores, favor de consultar suplemento. Precio del niño aplica cuando comparte habitación con 2 adultos. La mañana del día de Victoria es libre para los pasajeros y el guía los recogerá alrededor de las 16:30 hrs para regresar a Vancouver. El hotel Fairmont Empress se reemplazará las salidas de Julio 1, 29 y Septiembre 2 por el Hotel Grand Pacific
				</p>

				<div class="clearfix">
					<img src="img/bc.png" alt="" class="img-responsive pull-right">
				</div>

				<p class="tel">
					Llámanos: (55) 5901 2000 | 01 800 502 1500
				</p>
				<p class="cotizar">
					<a href="/reserva">Cotizar</a>
				</p>
	        </div>
	    </section>
	</main>

<?php require_once('footer.php'); ?>