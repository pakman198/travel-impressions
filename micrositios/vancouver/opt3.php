<?php require_once('header.php'); ?>

	<main class="container experiencia-detail">
	    <section class="header">
	        <div class="col-sm-8">
	            <div class="heading">
	            	<div class="title">
	            		VANCOUVER - VICTORIA ROCOSAS - CALGARY
	            		<div class="subtitle">Lo mejor de las Montañas Rocosas</div>
	            	</div>
	                <div class="pic-container">
	                	<img src="img/van3.jpg" alt="" class="img-responsive">
	                	<!-- <img src="img/vancouver.png" alt="" class="watermark"> -->
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-4">
	            <div class="excerpt">
	                <p class="strong">8 NOCHES</p>
	                <p>
	                	Salidas: domingos <br>
	                	Septiembre: 4, 11, 18, 25
	                </p>
	                <p class="strong">Código: YVR-13</p>
	            </div>
	            <div class="promo">
	            	<p>paga a 6 meses sin intereses</p>
	            	<img src="img/tarjetas.jpg" alt="" class="img-responsive">
	            </div>
	        </div>
	    </section>
	    <section class="description">
	        <div class="col-sm-12">
	            <h3>Costo total por persona / Temporada alta</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$3,503.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Triple</td>
	            			<td>$3,055.00</td>
	            		</tr>
	            		<tr>
	            			<td>Cuádruple</td>
	            			<td>$2,831.00</td>
	            		</tr>
	            		<tr>
	            			<td>Menor (Hasta 12)</td>
	            			<td>$1,846.00</td>
	            		</tr>
	            	</table>
	            </div>

	            <h3>*Suplemento Estampida Calgary</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>Doble</td>
	            			<td>$104.00</td>
	            			<td rowspan="4">
	            				Precios totales mencionados en Dolares Canadienses
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Triple</td>
	            			<td>$69.00</td>
	            		</tr>
	            		<tr>
	            			<td>Cuádruple</td>
	            			<td>$52.00</td>
	            		</tr>
	            		<tr>
	            			<td>Menor (Hasta 12)</td>
	            			<td>n/a</td>
	            		</tr>
	            	</table>
	            </div>

	            <h3>Hoteles programados o similares</h3>
	            <div class="table-responsive">
	            	<table class="table">
	            		<tr>
	            			<td>3</td>
	            			<td>VANCOUVER</td>
	            			<td>Fairmont Hotel Vancouver (Superior-Lujo)</td>
	            		</tr>
	            		<tr>
	            			<td>1</td>
	            			<td>KAMLOOPS</td>
	            			<td>South Thompson Inn (Rancho)</td>
	            		</tr>
	            		<tr>
	            			<td>2</td>
	            			<td>JASPER</td>
	            			<td>Fairmont Jasper Park Lodge (1) (Lodge)</td>
	            		</tr>
	            		<tr>
	            			<td>1</td>
	            			<td>LAKE LOUISE</td>
	            			<td>Fairmont Chateau Lake Louise (2) (Lujo)</td>
	            		</tr>
	            		<tr>
	            			<td>1</td>
	            			<td>CALGARY</td>
	            			<td>International Hotel (Primera)</td>
	            		</tr>
	            	</table>
	            </div>
	            
				<h3>Incluye</h3>

				<ul>
					<li>
						Traslados Aeropuerto - Hotel - Aeropuerto
					</li>
					<li>
						8 Desayunos americanos
					</li>
					<li>
						1 Cena en el Rancho South Thompson Inn
					</li>
					<li>
						1 Almuerzo en el Chalet de Maligne Lake
					</li>
					<li>
						1 Almuerzo en el Fairmont Banff Springs
					</li>
					<li>
						Transporte en grupos reducidos con chofer/guía en español
					</li>
					<li>
						Visitas incluidas: Tour de la Ciudad de Vancouver, Tour de Victoria, Butchart Gardens, Teleférico Hells Gate, Ice Explorer, Panoramica de Calgary
					</li>
					<li>
						Maleteros (1 pieza de equipaje por persona)
					</li>
					<li>
						Bolsa porta documentos por persona
					</li>
				</ul>

				<p class="legales">
					Legales <br>
					Hoteleria en hoteles mencionados o similares. Tipo de habitación: estándar. En caso de preferir habitaciones superiores, favor de consultar suplemento. Fairmont Chateau Lake Louise la habitación es estándar y no tiene vista al lago. Precio de niño aplica cuando comparte habitación con 2 adultos. *Fechas coincidentes con la Estampida de Calgary (hay suplemento). 1) El Fairmont Jasper Park Lodge se reemplazará por el Tekarra Lodge en habitación Cabin (consulte salida). (2) El Fairmont Chateau Lake Louise en Julio 24 y Agosto 21, se reemplazará por el Post Hotel & Spa (Relais & Chateaux Property).

				</p>

				<h4>CONSULTE ITINERARIO DETALLADO</h4>

				<div class="clearfix">
					<img src="img/bc.png" alt="" class="img-responsive pull-right">
				</div>

				<p class="tel">
					Llámanos: (55) 5901 2000 | 01 800 502 1500
				</p>
				<p class="cotizar">
					<a href="/reserva">Cotizar</a>
				</p>
	        </div>
	    </section>
	</main>

<?php require_once('footer.php'); ?>