'use strict'

var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');

gulp.task('styles', function () {
  return gulp.src('./less/main.less')
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
    .pipe(rename({
      basename: 'styles'
    }))
    //.pipe(sourcemaps.write('./', {addComment: true}))
    .pipe(gulp.dest('./css')) //write compiled styles
    .pipe(cleanCSS()) //minify styles
    .pipe(rename({
      suffix : '.min'
    }))
    .pipe(sourcemaps.write('./', {addComment: true}))
    .pipe(gulp.dest('./css'));
});

gulp.task('images', function() {
    return gulp.src('./img/*.jpg')
    .pipe(imagemin({ progressive: true }))
    .pipe(gulp.dest('images'));
});

// Watching files
gulp.task('watch', function() {
  var watcher = gulp.watch("less/**/*.less", {verbose:true}, ['styles']);

  watcher.on('change', function(event) {
    console.log('\nFile ' + event.path + ' was ' + event.type + ', running tasks...');
  });

});



// Run these tasks as default
gulp.task('default', ['styles']);





