<?php

//Template Name: Experiencias Contact
//Description: Formulario de contacto

/*error_reporting(E_ALL); 
ini_set('display_errors', 1);  
ini_set('max_execution_time', 10);  */
ini_set('sendmail_from', "pakman_198@hotmail.com");

require_once 'phpMailer/PHPMailerAutoload.php';
//header ( 'Content-Type: application/json;charset=utf-8' );
//header ( "Cache-Control: no-cache, must-revalidate" );

if( 
	$_POST['firstName'] != "" && 
	$_POST['lastName'] != "" && 
	$_POST['email'] != "" && 
	$_POST['phone'] != "" && 
	$_POST['experience'] != "" && 
	$_POST['startDate'] != "" && 
	$_POST['endDate'] != "" && 
	$_POST['adults'] != "" && 
	$_POST['underage'] != "" && 
	$_POST['category'] != "" && 
	$_POST['traslado'] != ""
	){

	$to = 'jessica.hernandez@americanexpressvacations.com';
	
	$subject = 'Nuevo mail del Experiencias por el Mundo';

	$headers = "From: " . strip_tags($_POST['email']) . "\r\n";
	//$headers = "Cc: " . strip_tags($_POST['email']) . "\r\n";
	$headers .= "Reply-To: ". strip_tags($_POST['email']) . "\r\n";
	
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

	if(isset($_POST['mobile'])){
		$tel = "<strong>Teléfono: </strong> " . $_POST['phone'] . "(Móvil) <br/>";
	}else{
		$tel  = "<strong>Teléfono: </strong> " . $_POST['phone'] . "<br/>";
	}
	if(isset($_POST['agency']) && $_POST['agency'] != ""){
		$agencia = "<strong>Agencia: </strong> " . $_POST['agency'] . "<br/>";
	}else{
		$agencia  = "";
	}
	if(isset($_POST['underage']) && $_POST['underage'] > 0){
		$menores = 
		"<strong>Menores: </strong> " . $_POST['underage'] . "<br/>"
		. "<strong>Edades de los Menores: </strong> " . $_POST['age'] . "<br/>";
	}else{
		$menores  = "";
	}
	if(isset($_POST['aereo'])){
		$aereo = 
		"<strong>Aereo: </strong> &#10003; <br/>";
	}else{
		$aereo  = "";
	}
	if(isset($_POST['additional']) && $_POST['additional'] != ""){
		$adicional = "<h2> Información Adicional </h2>"
			. $_POST['additional'];
	}else{
		$adicional  = "";
	}
	$message  = 
	"<h2> Información Personal </h2>"
	. "<strong>Nombre: </strong>" . $_POST['firstName'] . " " . $_POST['lastName'] . "<br/>" 
	. "<strong>Email: </strong>" . $_POST['email'] . "<br/>" 
	. $tel 
	. "<h2> Datos de Viaje </h2>"
	. $agencia
	. "<strong>Experiencia: </strong>" . $_POST['experience'] . "<br/>"
	. "<strong>Fecha de salida: </strong>" . $_POST['startDate'] . "<br/>"
	. "<strong>Fecha de regreso: </strong>" . $_POST['endDate'] . "<br/>"
	. "<strong>Adultos: </strong>" . $_POST['adults'] . "<br/>"
	. $menores
	. "<strong>Categoría del hotel: </strong>" . $_POST['category'] . "<br/>"
	. "<h2> Traslados </h2>"
	. "<strong>Tipo de traslado: </strong>" . $_POST['traslado'] . "<br/>"
	. $aereo
	. $adicional;

	if(@mail($to, $subject, $message,$headers)){
		$data['success'] = true;
		$data['message']="Mensaje enviado correctamente!";
		//echo json_encode($data);
		$response = "<div class='alert alert-success'>
	                 <button type='button' class='close' data-dismiss='alert'>×</button>" .
	                 "Tu mensaje fue enviado correctamente." . 
	                 "</div>";
	}
	else{
		$data['success'] = false;
		$data['message'] = "Ocurrió un error, intenta nuevamente";
		//echo json_encode($data);
		$response = "<div class='alert alert-error'>
	                 <button type='button' class='close' data-dismiss='alert'>×</button>" .
	                 "Lo sentimos, tu mensaje no pudo ser enviado." . 
	                 "</div>";
	}
}

	get_header(); 

?>

<!--
	Start of DoubleClick Floodlight Tag: Please do not remove
	Activity name of this tag: Cotizar
	URL of the webpage where the tag is expected to be placed: http://www.seetorontonow.com
	This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
	Creation Date: 07/19/2016
-->

<script type="text/javascript">
	var axel = Math.random() + "";
	var a = axel * 10000000000000;
	document.write('<iframe src="https://5880041.fls.doubleclick.net/activityi;src=5880041;type=invmedia;cat=5yhjz6dp;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>

<noscript>
	<iframe src="https://5880041.fls.doubleclick.net/activityi;src=5880041;type=invmedia;cat=5yhjz6dp;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

<main class="container contact">
	<div class="row">
		<div class="col-sm-12">
			<?php echo $response; ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<form action="" name="contactForm" method="POST">
				<fieldset>
					<legend>Datos Personales</legend>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="name">Nombre:</label>
								<input type="text" name="firstName" class="form-control" required>
							</div>		
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="lastName">Apellido:</label>
								<input type="text" name="lastName" class="form-control" required>
							</div>	
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="email">Email:</label>
								<input type="email" name="email" class="form-control" required>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<label for="phone">Teléfono</label>
										<input type="text" name="phone" class="form-control" placeholder="10 Digitos" required>
									</div>	
								</div>
								<div class="col-md-4">
									<div class="checkbox">
										<label for="mobile">
											<input type="checkbox" name="mobile"> Es número móvil
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Datos de Viaje</legend>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="agency">Agencia:</label>
								<input type="text" name="agency" class="form-control">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="experience">Experiencia a cotizar:</label>
								<input type="text" name="experience" class="form-control" required>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="dates">Fechas de viaje:</label>
								<div class="row">
									<div class="col-sm-6">
										<input type="date" name="startDate" class="form-control" placeholder="dd/mm/aaaa" required>		
									</div>
									<div class="col-sm-6">
										<input type="date" name="endDate" class="form-control" placeholder="dd/mm/aaaa" required>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="pasajeros">Cantidad de pasajeros:</label>
								<div class="row">
									<div class="col-sm-4">
										<input type="number" min="0" name="adults" class="form-control" placeholder="Adultos" required>
									</div>
									<div class="col-sm-4">
										<input type="number" min="0" name="underage" class="form-control" placeholder="Menores" required>
									</div>
									<div class="col-sm-4">
										<input type="text" name="ages" class="form-control" placeholder="Edad de los menores">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="category">Categoría del hotel:</label>
								<input type="text" name="category" class="form-control" required>
							</div>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Traslados</legend>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="traslado">Tipo de traslados:</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="traslado" id="optionsRadios1" value="Regular" required> Regular
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="traslado" id="optionsRadios1" value="Privado"> Privado
								</label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="checkbox">
								<label for="aereo">
									<input type="checkbox" name="aereo"> Aéreo (ciudad de origen)
								</label>
							</div>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Información Adicional</legend>
					<textarea name="additional" id="additional" cols="30" rows="6" class="form-control"></textarea>
				</fieldset>

				<p class="text-center"><input type="submit" class="btn btn-primary btn-lg" value="Enviar"></p>

			</form>
		</div>
	</div>
</main>

<!-- Google Code for Cotizar Conversion Page -->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 877708376;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "zzU9COnjymgQ2IjDogM";
	var google_remarketing_only = false;
	/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
	<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/877708376/?label=zzU9COnjymgQ2IjDogM&amp;guid=ON&amp;script=0"/>
	</div>
</noscript>


<?php get_footer(); ?>