<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Travel Impressions</title>
	<link href="<?php bloginfo('template_url'); ?>/trav-imp.css" rel="stylesheet">
</head>
<body class="front-page">
	<nav class="navbar navbar-default mobile-nav visible-xs">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php bloginfo('url'); ?>" class="navbar-brand">
					<img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="logo" class="img-responsive">
				</a>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<?php 
					$defaults = array(
						'container' => false,
						'theme_location' => 'primary-menu'
					);
					wp_nav_menu( $defaults ); 
				?>
			</ul>
		</div>
	</nav>
	<header class="home">
		<img src="<?php bloginfo('template_url'); ?>/img/home.jpg" alt="">

		<nav class="hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="logo" class="img-responsive"></a>
					</div>
					<div class="col-sm-8">
						<ul class="list-inline text-right">
							<?php 
								$defaults = array(
									'container' => '',
									'container_class' => false,
									'theme_location' => 'primary-menu'
								);
								wp_nav_menu( $defaults ); 
							?>
						</ul>	
					</div>
				</div>
			</div>
		</nav>
	</header>
	<main class="home-content">
		<div class="opacity"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<h2 class="title">
						En Travel Impressions contamos con más de 40 años de experiencia para poder ofrecerte:
					</h2>		
				</div>
			</div>
			
			<div class="row strengths">
				<div class="col-sm-6 col-md-3 border">
					<p>
						Un servicio confiable y de calidad, brindado por expertos consejeros de viajes.
					</p>
				</div>
				<div class="col-sm-6 col-md-3 border">
					<p>
						La mejor selección de viajes de experiencias por el mundo, trabajamos con los mejores operadores locales.
					</p>
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="col-sm-6 col-md-3 border">
					<p>
						Precios competitivos, pagos con Puntos Payback y con cualquier tarjeta bancaria.
					</p>
				</div>
				<div class="col-sm-6 col-md-3 border">
					<p>
						Un área de Customer Care para asegurarnos que no tengas imprevistos durante tu viaje.
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<h2 class="motto">
						Bienvenido al mundo de Travel Impressions
					</h2>
					<h2 class="motto text-right">Llamanos: (55) 5901 2000 | 01 800 502 5000</h2>
				</div>
			</div>
		</div>
	</main>

	<?php get_footer(); ?>