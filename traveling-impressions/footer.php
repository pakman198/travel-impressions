	<footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-3 hidden-xs">
					<img src="<?php bloginfo('template_url'); ?>/img/logo-white.png" alt="logo" class="logo img-responsive">
				</div>
				<div class="col-sm-9">
					<p class="text-center">
						<?php 
							$defaults = array(
								'container' => '',
								'container_class' => false,
								'theme_location' => 'footer-menu',
								'items_wrap' => '%3$s',
								'echo' => false
							);
							echo strip_tags( wp_nav_menu( $defaults ), '<a>');
						?>
					</p>
					<p class="text-center">
						<span>copyright&copy; travel impressions</span><span>llámanos: (55) 5901 2000</span><span>01 800 502 5000</span>
					</p>
				</div>
			</div>
		</div>
	</footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-79086501-1', 'auto');
		ga('send', 'pageview');
	</script>

</body>
</html>