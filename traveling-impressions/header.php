<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Travel Impression</title>
	<link href="<?php bloginfo('template_url'); ?>/trav-imp.css" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-default mobile-nav visible-xs">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php bloginfo('url'); ?>" class="navbar-brand">
					<img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="logo" class="img-responsive">
				</a>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<?php 
					$defaults = array(
						'container' => false,
						'theme_location' => 'primary-menu'
					);
					wp_nav_menu( $defaults ); 
				?>
			</ul>
		</div>
	</nav>
	<header>
		<nav class="hidden-xs">
			<div class="container">
				<div class="row main-menu">
					<div class="col-sm-3">
						<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo-white.png" alt="logo" class="img-responsive"></a>
					</div>
					<div class="col-sm-9">
						<ul class="list-inline text-right">
							<?php 
								$defaults = array(
									'container' => false,
									'theme_location' => 'primary-menu'
								);
								wp_nav_menu( $defaults ); 
							?>
						</ul>	
					</div>
				</div>
			</div>
		</nav>
	</header>