<?php 
	get_header();

	get_template_part('experiencias', 'header');
?>

<div class="container experiencia-home">
	<div class="row">
		
		<?php if(have_posts()) : while (have_posts()) : the_post(); ?>

		    <article class="col-sm-6">
		    	<a href="<?php the_permalink(); ?>">
					<div class="thumbnail">
						<?php the_post_thumbnail('large'); ?>
					</div>
					<div class="description">
						<h1 class="title"><?php the_title(); ?></h1>
						<h2 class="quote"><?php the_excerpt(); ?></h2>
					</div>
				</a>
		    </article>

		<?php endwhile; else : ?>
			<div class="col-sm-8 col-sm-offset-2">
				<h1 class="error text-center">No se encontraron experiencias relacionadas con tu búsqueda</h1>
			</div>
		<?php endif; ?>
	</div>
</div>

<?php get_footer();?>