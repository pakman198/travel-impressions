<?php

function experiencias_setup() {

    add_theme_support( 'post-thumbnails' );

    set_post_thumbnail_size( 624, 200 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', 'experiencias_setup' );


//Register area for custom menu
function register_my_menu() {
	register_nav_menu( 'primary-menu', __( 'Primary Menu' ) );
	register_nav_menu( 'footer-menu', __( 'Terms & Conditions Menu' ) );
}
add_action( 'init', 'register_my_menu' );

// remove ul wp_nav_menu
function remove_ul ( $menu ){
    return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}
add_filter( 'wp_nav_menu', 'remove_ul' );



/*******************************************************/
/****************** Category Colors ********************/
/*******************************************************/

/** Add Custom Field To Category Form */

add_action( 'category_add_form_fields', 'category_form_custom_field_add', 10 );
add_action( 'category_edit_form_fields', 'category_form_custom_field_edit', 10, 2 );

function category_form_custom_field_add( $taxonomy ) {
	$category_colors = array(
	    'red' => 'Red',
	    'brown' => 'Brown',
	    'lightGrue' => 'Light Grue',
	    'purple' => 'Purple',
	    'lightGreen' => 'Light Green',
	    'yellow' => 'Yellow',
	    'grue' => 'Grue',
	    'orange' => 'Orange',
	    'green' => 'Green',
	    'pink' => 'Pink'
	);

	?>
	<div class='form-field'>
		<label for='category_custom_color'>Color</label>
		<select name='category_custom_color' id='category_custom_color'>
			<?php foreach ($category_colors as $k => $v) : ?>
				<option value="<?php echo $k;?>"><?php echo $v;?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<?php
}
 
function category_form_custom_field_edit( $tag, $taxonomy ) {
	$category_colors = array(
	    'red' => 'Red',
	    'brown' => 'Brown',
	    'lightGrue' => 'Light Grue',
	    'purple' => 'Purple',
	    'lightGreen' => 'Light Green',
	    'yellow' => 'Yellow',
	    'grue' => 'Grue',
	    'orange' => 'Orange',
	    'green' => 'Green',
	    'pink' => 'Pink'
	);
    $option_name = 'category_custom_color_' . $tag->term_id;
    $color = get_option( $option_name );
	?>
	
	<tr class='form-field'>
	  <th scope='row' valign='top'>
	  	<label for='category_custom_color'>Color</label>
	  </th>
	  <td>
	    <select name='category_custom_color' id='category_custom_color'>
	    	<?php 
	    		foreach($category_colors as $key => $value) : 
	    			$selected = ($key == $color) ? "selected='selected'" : '';
	    		echo $key;
	    	?>
				<option value="<?php echo $key;?>" <?php echo $selected; ?> > 
					<?php echo $value; ?>
				</option>
			<?php endforeach; ?>
		</select>
	  </td>
	</tr>
	<?php
}
 
/** Save Custom Field Of Category Form */
add_action( 'created_category', 'category_form_custom_field_save', 10, 2 ); 
add_action( 'edited_category', 'category_form_custom_field_save', 10, 2 );

function category_form_custom_field_save( $term_id, $tt_id ) {
 
    if ( isset( $_POST['category_custom_color'] ) ) {           
        $option_name = 'category_custom_color_' . $term_id;
        update_option( $option_name, $_POST['category_custom_color'] );
    }
}

// Add the custom color to the category table
add_filter('manage_edit-category_columns', 'admin_show_category_color', 10, 2);
add_action( "manage_category_custom_column", 'custom_column_content', 10, 3);

function admin_show_category_color($cat_columns){
	echo $cat_columns;
    $cat_columns['custom_color'] = 'Color';
    return $cat_columns;
}

function custom_column_content( $value, $column_name, $tax_id ){
	$term_meta = get_option( "category_custom_color_".$tax_id );
	return $term_meta;
}




