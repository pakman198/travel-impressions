<?php get_header(); ?>

	<main class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1 class="error error-page text-center">
					Lo sentimos, no se encontró la página a la que intentas acceder.
				</h1>
			</div>
		</div>
	</main>

<?php get_footer(); ?>