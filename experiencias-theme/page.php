<?php get_header(); ?>

<div class="container page">
	<div class="row">
		<div class="col-sm-12">
			
			<?php if (have_posts()) : ?><?php while (have_posts()) : the_post(); ?>

			    <?php the_content(); ?>

			<?php endwhile; endif; ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>