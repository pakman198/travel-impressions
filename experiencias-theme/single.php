<?php 
    get_header();

    get_template_part('experiencias', 'header');
?>

<?php while (have_posts()) : the_post(); ?>

    <main class="container experiencia-detail">
        <section class="row">
            <div class="col-sm-8">
                <div class="heading">
                    <div class="title"><?php the_title(); ?></div>
                    <?php the_post_thumbnail('full', array('class'=>'img-responsive') ); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="excerpt">
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </section>
        <section class="row description">
            <div class="col-sm-12">
                <?php the_content(); ?>
            </div>
        </section>
    </main>

<?php endwhile; ?>


<?php get_footer();?>