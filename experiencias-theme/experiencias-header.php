<?php
	if (is_category( )) {
		$category = get_query_var('cat');
		$cat = get_category ($category);
		$color = get_option('category_custom_color_' . $cat->cat_ID);
	}elseif(is_single()){
		while(have_posts()){
			the_post();

			$category = get_the_category();
			$cat = $category[0];
			$color = get_option('category_custom_color_' . $cat->cat_ID);
		}
	}
?>

<!-- <pre>
	<?php print_r($cat); ?>
</pre> -->

<div class="experiencias-header">
	<div class="experiencia <?php echo $cat->slug;?>">
		<div class="mask bg-<?php echo $color;?>"></div>
		<img src="<?php bloginfo('template_url'); echo '/img/' . $cat->slug;?>.jpg" alt="">
		<div class="container">
			<div class="title-wrapper">
				<a href="<?php bloginfo('url'); ?>/category/experiencias/<?php echo $cat->slug; ?>">
					<h2 class="title">
						<?php echo $cat->name; ?>
					</h2>
				</a>
			</div>
		</div>
	</div>
</div>