<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>American Express Vacations</title>
	<link href="<?php bloginfo('template_url'); ?>/experiencias.css" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-default mobile-nav visible-xs">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php bloginfo('url'); ?>" class="navbar-brand">
					<img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="logo" class="img-responsive">
				</a>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<?php 
					$defaults = array(
						'container' => false,
						'theme_location' => 'primary-menu'
					);
					wp_nav_menu( $defaults ); 
				?>
			</ul>
		</div>
	</nav>
	<header class="home">
		<img src="<?php bloginfo('template_url'); ?>/img/home.jpg" alt="">

		<nav class="hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="logo" class="img-responsive"></a>
					</div>
					<div class="col-sm-8">
						<ul class="list-inline text-right">
							<?php 
								$defaults = array(
									'container' => '',
									'container_class' => false,
									'theme_location' => 'primary-menu'
								);
								wp_nav_menu( $defaults ); 
							?>
						</ul>	
					</div>
				</div>
			</div>
		</nav>
	</header>
	<main class="home-content">
		<div class="ribbon"></div>
		<div class="container">
			<div id="carousel" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
			    
					<div class="item active">
						<img src="<?php bloginfo('template_url'); ?>/img/Home_1.jpg" alt="">
						<div class="carousel-caption hidden-xs">
							El gran cañon te robará el corazón
						</div>
					</div>
					<div class="item">
						<img src="<?php bloginfo('template_url'); ?>/img/Home_2.jpg" alt="">
						<div class="carousel-caption hidden-xs">
							El cine en su estado puro
						</div>
					</div>
					<div class="item">
						<img src="<?php bloginfo('template_url'); ?>/img/Home_3.jpg" alt="">
						<div class="carousel-caption hidden-xs">
							Disfruta la diversión y actividades en moon palace
						</div>
					</div>
					<div class="item">
						<img src="<?php bloginfo('template_url'); ?>/img/Home_4.jpg" alt="">
						<div class="carousel-caption hidden-xs">
							Disfruta de espectaculares vistas al mar de cortés
						</div>
					</div>
					<div class="item">
						<img src="<?php bloginfo('template_url'); ?>/img/Home_5.jpg" alt="">
						<div class="carousel-caption hidden-xs">
							Disfruta un pedacito del paraíso natural
						</div>
					</div>
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
	</main>

	<?php get_footer(); ?>