<?php

//Template Name: Experiencias Landing
//Description: Landing page para las experiencias (categorias)

	get_header(); 

?>

<main class="experiencias-landing">
	<div class="experiencia">
		<div class="mask bg-red"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/adrenalina.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/adrenalina">
				<h2 class="title">Adrenalina</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-brown"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/arte.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/arte">
				<h2 class="title">Arte, Cultura e Historia</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-lightGrue"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/cine.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/cine">
				<h2 class="title">Cine</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-purple"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/compras.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/compras">
				<h2 class="title">Compras</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-lightGreen"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/deportes.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/deportes">
				<h2 class="title">Deportes</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-yellow"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/entretenimiento.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/entretenimiento">
				<h2 class="title">Entretenimiento</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-grue"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/eventos.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/eventos">
				<h2 class="title">Eventos y Festivales</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-orange"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/gastronomia.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/gastronomia">
				<h2 class="title">Gastronomía y Vinos</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-green"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/naturaleza.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/naturaleza">
				<h2 class="title">Naturaleza</h2>
			</a>
		</div>
	</div>
	<div class="experiencia">
		<div class="mask bg-pink"></div>
		<img src="<?php bloginfo('template_url'); ?>/img/romance.jpg" alt="">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/category/experiencias/romance">
				<h2 class="title">Romance</h2>
			</a>
		</div>
	</div>
</main>

<?php get_footer(); ?>